import urllib.request
import json
import csv
import webbrowser
import gi
import requests
from gi.repository import Notify, GdkPixbuf, GLib
from youtube_dl import YoutubeDL

gi.require_version('Notify', '0.7')

def get_all_video_in_channel(channel_id):
    # https://stackoverflow.com/questions/14366648/how-can-i-get-a-channel-id-from-youtube#16326307
    # https://commentpicker.com/youtube-channel-id.php
    print("[INFO] Searching for latest vid with channel id {}".format(channel_id))

    api_key = ""

    base_video_url = 'https://www.youtube.com/watch?v='
    base_search_url = 'https://www.googleapis.com/youtube/v3/search?'

    first_url = base_search_url+'part=snippet&channelId={}&maxResults=10&order=date&type=video&key={}'.format(channel_id, api_key)

    video_links = []
    url = first_url
    inp = urllib.request.urlopen(url)
    resp = json.load(inp)

    for i in resp['items']:
        if i['id']['kind'] == "youtube#video":
            video_links.append(base_video_url + i['id']['videoId'])

    try:
        next_page_token = resp['nextPageToken']
        url = first_url + '&pageToken={}'.format(next_page_token)
    except:
        return "Error"

    print("[INFO] FOUND! Link {}".format(video_links[0]))
    return video_links[0] # we return the first index to get the most recent vid

def loadDict():
    # dont even ask
    
    youtubersDict = {}
    lastLink = {}
    
    with open("youtubers.txt", "r") as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',')
        lastLink = {rows[0]:rows[2] for rows in csvreader}
        csvfile.close()
    
    with open("youtubers.txt", "r") as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',')
        youtubersDict = {rows[0]:rows[1] for rows in csvreader}
        csvfile.close()
        
    return youtubersDict, lastLink

def callback(URL):
    webbrowser.open(URL)

def showNoti(channelName, videoName, URL):
    Notify.init("New Youtube Video")
    image = GdkPixbuf.Pixbuf.new_from_file("youtube.png")
    channelName = channelName + " just uploaded!"
    
    n = Notify.Notification.new(channelName,
                                videoName,
                                "Go Watch it!")
    n.add_action("clicked", "Watch", callback, URL)
    n.set_image_from_pixbuf(image)
    
    n.set_timeout(0)
    n.show()
    #Notify.uninit()

def getTitle(URL):
    video_title = ""
    youtube_dl_opts = {}

    with YoutubeDL(youtube_dl_opts) as ydl:
        info_dict = ydl.extract_info(URL, download=False)
        video_title = info_dict.get('title', None)

    return video_title

def writeToFile(oldLink, newLink):
    text = open("youtubers.txt", "r")
    text = ''.join([i for i in text]) \
        .replace(oldLink, newLink)
    x = open("youtubers.txt","w")
    x.writelines(text)
    x.close()
    print("[INFO] Successfully replaced {} with {} in file.".format(oldLink, newLink))

def main():
    youtuberDict = loadDict()[0]
    youtuberDictKey = list(youtuberDict.keys())
    lastLink = loadDict()[1]
    lastLinkKey = list(lastLink.keys())

    for i in range(len(youtuberDict)):
        getLink = get_all_video_in_channel(youtuberDict[youtuberDictKey[i]])
        
        if getLink == lastLink[lastLinkKey[i]]:
            # no new vid
            print("[INFO] no new vid for channel: {}".format(youtuberDictKey[i]))
        else:
            # new vid
            print("[INFO] new vid for channel: {}".format(youtuberDictKey[i]))

            showNoti(youtuberDictKey[i], getTitle(getLink), getLink)

            writeToFile(lastLink[lastLinkKey[i]], getLink)
main()